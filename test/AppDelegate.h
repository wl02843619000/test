//
//  AppDelegate.h
//  test
//
//  Created by ES711-apple-2 on 2019/5/3.
//  Copyright © 2019 ES711-apple-2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

