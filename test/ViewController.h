//
//  ViewController.h
//  test
//
//  Created by ES711-apple-2 on 2019/5/3.
//  Copyright © 2019 ES711-apple-2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;




@end

