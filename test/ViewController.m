//
//  ViewController.m
//  test
//
//  Created by ES711-apple-2 on 2019/5/3.
//  Copyright © 2019 ES711-apple-2. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){
    UILayoutGuide *margins;
    UIView *gameView, *instructionView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    margins = self.view.safeAreaLayoutGuide;
    
    [self configScrollView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeRotate:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    
    self.view.backgroundColor = [UIColor colorWithRed:33/255.0 green:31/255.0 blue:92/255.0 alpha:1.0];

}

- (void)viewDidAppear:(BOOL)animated{
    
    
    //在viewDidAppear裡面設定 scrooview gameview instruction 的 margins.size
    [self setViewSize];
    
}

- (void)configScrollView{
    
    //scrollView initialize
    [_scrollView setBounces:NO];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_scrollView.topAnchor constraintEqualToAnchor:margins.topAnchor].active = YES;
    [_scrollView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
    [_scrollView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
    [_scrollView.bottomAnchor constraintEqualToAnchor:margins.bottomAnchor].active = YES;
    
    
    //scrollView 新增（呼叫） xib
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"vertical" owner:nil options:nil];
    gameView = [array lastObject];
    [_scrollView addSubview:gameView];
    
    //新增 gameView 對 scrollView top Constraints
    gameView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem: gameView
                                                          attribute: NSLayoutAttributeTop
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: _scrollView
                                                          attribute: NSLayoutAttributeTop
                                                         multiplier: 1.0
                                                           constant: 0
                              ]
     ];
    
    //gameView Constraints
    [gameView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
    [gameView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
    [gameView.heightAnchor constraintEqualToConstant:margins.layoutFrame.size.height].active = YES;
    
    //scrollView add instruction View
    NSArray *array2 =[[NSBundle mainBundle]loadNibNamed:@"instruction" owner:nil options:nil];
    instructionView = [array2 lastObject];
    [_scrollView addSubview:instructionView];
    
    //instruction to gameView Top Constraints
    instructionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem: instructionView
                                                          attribute: NSLayoutAttributeTop
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: gameView
                                                          attribute: NSLayoutAttributeBottom
                                                         multiplier: 1.0
                                                           constant: 0
                              ]
     ];
    
    //instructionView Constraints
    [instructionView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
    [instructionView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
    
}

- (void)setViewSize{
    _scrollView.contentSize = CGSizeMake(0, margins.layoutFrame.size.height * 2 );
    [_scrollView layoutIfNeeded];
    [gameView.heightAnchor constraintEqualToConstant:margins.layoutFrame.size.height].active = YES;
    [instructionView.heightAnchor constraintEqualToConstant:margins.layoutFrame.size.height].active = YES;
    
    NSLog(@"viewDidAppear safeAreaLayoutGuide wid = %f, hei = %f", margins.layoutFrame.size.width, margins.layoutFrame.size.height);
}

//屏幕翻轉呼叫
- (void)changeRotate:(NSNotification*)notify{
    //重新判斷safe area
    [self setViewSize];
    
    [gameView removeFromSuperview];
    [instructionView removeFromSuperview];
    
    if ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortrait
        || [[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortraitUpsideDown) {
        NSLog(@"Portrait");

        NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"vertical" owner:nil options:nil];

        gameView = [array lastObject];

        [self.scrollView addSubview:gameView];
        [self.scrollView addSubview:instructionView];

    } else {
        //横屏
        NSLog(@"Horizontal");

        NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"horizontal" owner:nil options:nil];

        gameView = [array lastObject];

//        [self.scrollView insertSubview:gameView aboveSubview:instructionView];
        [self.scrollView addSubview:gameView];
        [self.scrollView addSubview:instructionView];
    }
    
    [self viewconfig];
}

- (void)viewconfig{
    //新增 gameView 對 scrollView top Constraints
    gameView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem: gameView
                                                          attribute: NSLayoutAttributeTop
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: _scrollView
                                                          attribute: NSLayoutAttributeTop
                                                         multiplier: 1.0
                                                           constant: 0]];
    
    //gameView Constraints
    [gameView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
    [gameView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
    [gameView.heightAnchor constraintEqualToConstant:margins.layoutFrame.size.height].active = YES;
    
    //instruction to gameView Top Constraints
    instructionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem: instructionView
                                                          attribute: NSLayoutAttributeTop
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: gameView
                                                          attribute: NSLayoutAttributeBottom
                                                         multiplier: 1.0
                                                           constant: 0]];
    
    //instructionView Constraints
    [instructionView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
    [instructionView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
    
}

@end
